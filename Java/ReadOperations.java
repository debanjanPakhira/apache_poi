package excelOperations;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;

public class ReadOperations {

	/**
	 * use this method to read the complete excel file
	 * 
	 * @param filePath
	 * @param sheetName
	 * @throws IOException
	 */
	public void readCompleteExcel(String filePath, String sheetName) throws IOException {
		System.out.println("*Printing complete worksheet data of: " + sheetName + "*\n");

		/*
		 * Step - 1 : Read the excel file using FileInputStream to obtain input bytes from a file.
		 * a. Create the object of File b. Create the object of FileInputStream
		 */
		File fileName = new File(filePath);
		FileInputStream file = new FileInputStream(fileName);


		/* Step - 2 : Create Workbook instance holding reference to .xlsx file */
		XSSFWorkbook workbook = new XSSFWorkbook(file);


		/*
		 * Step - 3 : Get first/desired sheet from the workbook
		 */
		XSSFSheet sheet = workbook.getSheet(sheetName);

		/* Step - 4 : Get the last row number */
		int lastRowNum = sheet.getLastRowNum();

		/*
		 * Step - 5 : Get the last cell number
		 */
		int columnNum = sheet.getRow(1).getLastCellNum();

		/*
		 * Step - 6 : Use a for each loop to iterate the row a. get the row b. using for each loop
		 * iterate over Cell of the row c. using switch statement check Cell type d. print the cell
		 * value
		 *
		 */
		for(int i=0; i<=lastRowNum; i++){
			XSSFRow row = sheet.getRow(i);
			for(int j=0; j<columnNum; j++){
				XSSFCell cell = row.getCell(j);
        switch (cell.getCellType()) {
            case STRING:
                System.out.print(cell.getStringCellValue());
                break;
            case NUMERIC:
                System.out.print(cell.getNumericCellValue());
                break;
            case BOOLEAN:
                System.out.print(cell.getBooleanCellValue());
                break;
            default:
                break;
        }
        System.out.print(" | ");
			}
			System.out.println();
		}

		System.out.println("\n");
	}

	/**
	 * use this method to read the row values from excel
	 * 
	 * @param filePath
	 * @param sheetName
	 * @param rowIndex
	 * @throws IOException
	 */
	public void getRowValue(String filePath, String sheetName, int rowIndex) throws IOException {
		/*
		 * Step - 1 : Read the excel file using FileInputStream to obtain input bytes from a file.
		 * a. Create the object of File b. Create the object of FileInputStream
		 */

		System.out.println("*Printing data in row no. " + rowIndex + " of: " + sheetName + "*\n");
		File filename = new File(filePath);
		FileInputStream file = new FileInputStream(filename);


		/* Step - 2 : Create Workbook instance holding reference to .xlsx file */
        XSSFWorkbook workbook = new XSSFWorkbook(file);
		/*
		 * Step - 3 : Get first/desired sheet from the workbook
		 */
        XSSFSheet worksheet = workbook.getSheet(sheetName);

		/* Step - 4 : Get the desire row */
        XSSFRow row = worksheet.getRow(rowIndex);
		/*
		 * Step - 5 : Iterate over over each Cell using for each loop a. using switch statement
		 * check Cell type b. print the cell value
		 */
		System.out.print(rowIndex + " Row values: " + "|");
		for(int i=0; i<row.getLastCellNum(); i++){
			XSSFCell cell = row.getCell(i);
			switch(cell.getCellType()){
				case STRING:
				System.out.print(cell.getStringCellValue()+"|");
                break;

				case NUMERIC:
				System.out.println(cell.getNumericCellValue()+"|");
				break;

				case BOOLEAN:
				System.out.println(cell.getBooleanCellValue()+"|");
				break;

				default:
				break;
			}
			
		}
		System.out.println("\n");
	}

	/**
	 * use this method to read column value
	 *
	 * @param filePath
	 * @param sheetName
	 * @param columnIndex
	 * @throws IOException
	 */
	public void getColunmValue(String filePath, String sheetName, int columnIndex)throws IOException {
		System.out.println("*Printing data in col no. " + columnIndex + " of: " + sheetName + "*\n");


		/*
		 * Step - 1 : Read the excel file using FileInputStream to obtain input bytes from a file.
		 * a. Create the object of File b. Create the object of FileInputStream
		 */
        File fileName = new File(filePath);
		FileInputStream file = new FileInputStream(fileName);

		/* Step - 2 : Create Workbook instance holding reference to .xlsx file */
		XSSFWorkbook workbook = new XSSFWorkbook(file);

		/*
		 * Step - 3 : Get first/desired sheet from the workbook
		 */
          XSSFSheet worksheet = workbook.getSheet(sheetName);
		/*
		 * Step - 4 : Using for each loop iterate over each row a. using for each loop iterate over
		 * each Cell of row b. Compare the column index for which you want print the values. if,
		 * match found print the cell value
		 */
		for(int i =0; i<=worksheet.getLastRowNum(); i++){
			XSSFCell cell = worksheet.getRow(i).getCell(columnIndex);
			switch(cell.getCellType()){
				case STRING:
				System.out.print("|"+cell.getStringCellValue()+"|\n");
                break;

				case NUMERIC:
				System.out.println("|"+cell.getNumericCellValue()+"|\n");
				break;

				case BOOLEAN:
				System.out.println("|"+cell.getBooleanCellValue()+"|\n");
				break;

				default:
				break;
			}

		}
		System.out.println("\n");
	}

	/**
	 * use this method to read a particular Cell value
	 * 
	 * @param filePath
	 * @param sheetName
	 * @param rowIndex
	 * @param colIndex
	 * @throws IOException
	 */
	public void getCellValue(String filePath, String sheetName, int rowIndex, int colIndex)
			throws IOException {
				System.out.println("*Printing data in cell of row no. " + rowIndex + " col no. " + colIndex
				+ " of: " + sheetName + "*\n");

		/*
		 * Step - 1 : Read the excel file using FileInputStream to obtain input bytes from a file.
		 * a. Create the object of File b. Create the object of FileInputStream
		 */
		File fileName = new File(filePath);
		FileInputStream file = new FileInputStream(fileName);

		/* Step - 2 : Create Workbook instance holding reference to .xlsx file */

		/*
		 * Step - 3 : Get first/desired sheet from the workbook
		 */

		/*
		 * Step - 4 : Get the row from which you want to read the cell data
		 */
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		XSSFSheet sheet = workbook.getSheet(sheetName);
		XSSFRow row = sheet.getRow(rowIndex);
		XSSFCell cell = row.getCell(colIndex);

		/* Step - 5 : Get the Cell value by passing the column index */

		/* Step - 6 : Print the cell value */
		switch (cell.getCellType()) {
			case STRING:
				System.out.print(cell.getStringCellValue());
				break;
			case NUMERIC:
				System.out.print(cell.getNumericCellValue());
				break;
			case BOOLEAN:
				System.out.print(cell.getBooleanCellValue());
				break;
			default:
				break;
		}
		System.out.println("\n");
	}

	public void run()  {
		String filePath = System.getProperty("user.dir") + "/src/main/resources/Activity.xlsx";
		String worksheetName = "Country Population";
		// Call the desired methods
		try {
			readCompleteExcel(filePath, worksheetName);

			getRowValue(filePath, worksheetName, 1);

			getColunmValue(filePath, worksheetName, 1);

			getCellValue(filePath, worksheetName, 3, 1);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
